﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;

namespace Pidroh.UnityUtils.Misc
{
    public class Countdown : MonoBehaviour
    {

        [SerializeField]
        float seconds = -1;

        bool paused = true;

        public float timeScale = 1;


        public UnityEvent timeUpEvent = new UnityEvent();

        public float Seconds
        {
            get
            {
                return seconds;
            }

            set
            {
                seconds = value;
            }
        }

        public bool Paused
        {
            get
            {
                return paused;
            }

            set
            {
                paused = value;
            }
        }

        // Use this for initialization
        void Start()
        {

        }

        public void Interrupt()
        {
            paused = true;
        }

        public void StartCount(float time)
        {

            seconds = time;
            paused = !(seconds > 0);
        }


        // Update is called once per frame
        void Update()
        {
            if (!Paused)
            {
                if (seconds >= 0)
                {
                    var dT = Time.deltaTime;

                    Seconds -= dT * timeScale;

                    if (seconds <= 0)
                    {
                        timeUpEvent.Invoke();
                    }

                }
            }


        }

    }
}