﻿namespace Pidroh.UnityUtils.Misc
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class ToggleGroupAux : MonoBehaviour
    {

        public ToggleGroup toggleGroup;
        public Transform toggleParent;
        [SerializeField]
        private int currentToggle;
        public IntEvent OnActiveToggleChange;

        //Canvas canvas;

        // Use this for initialization
        void Start()
        {
            
            var toggles = toggleParent.GetComponentsInChildren<Toggle>();
            for (int i = 0; i < toggles.Length; i++)
            {
                int toggleId = i;
                toggles[i].gameObject.name = toggleGroup.name + "-" + i;
                toggles[i].onValueChanged.AddListener((b) =>
                {
                    ToggleValueChanged(b, toggleId);
                });
            }
            //toggleGroup.
        }

        private void ToggleValueChanged(bool toggleOn, int which)
        {
            
            if (toggleOn)
            {
                currentToggle = which;
            }
            else {
                if (currentToggle == which) {
                    currentToggle = -1;
                }
            }
            OnActiveToggleChange.Invoke(currentToggle);
        }

        // Update is called once per frame
        void Update()
        {

        }

        internal void VisibleToggles(int amountOfOptions)
        {
            var toggles = toggleParent.GetComponentsInChildren<Toggle>();
            for (int i = 0; i < toggles.Length; i++)
            {

                toggles[i].gameObject.SetActive(i < amountOfOptions);
               
            }
        }

        internal void SetSelectedToggle(int currentlySelected)
        {
            var toggles = toggleParent.GetComponentsInChildren<Toggle>();
            for (int i = 0; i < toggles.Length; i++)
            {

                toggles[i].isOn = (i == currentlySelected);

            }

            ToggleValueChanged(true, currentlySelected);
        }
    }
}