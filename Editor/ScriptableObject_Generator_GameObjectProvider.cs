﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Pidroh.UnityUtils.Misc
{
    public class ScriptableObject_Generator_GameObjectProvider : EditorWindow
    {

        [MenuItem("Utilities/ScriptableAssets/GameObjectProvider_AutomaticGenerator")]

        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(ScriptableObject_Generator_GameObjectProvider));
        }

        void OnGUI()
        {

            //assetName = EditorGUILayout.TextField("S. Asset Name:", assetName);

            //target = EditorGUILayout.ObjectField("Label:", target, typeof(GameObject), true) as GameObject;

            bool ready = false;
            var gameObjects = Selection.gameObjects;
            if (gameObjects == null || gameObjects.Length == 0)
            {
                EditorGUILayout.LabelField("No objects selected");
            }
            else
            {
                var gobj = gameObjects[0];
                if (PrefabUtility.GetPrefabType(gobj) == PrefabType.Prefab)
                {
                    EditorGUILayout.LabelField(gameObjects.Length + " prefab(s) selected");
                    ready = true;
                }
                else
                {
                    EditorGUILayout.LabelField("Objects selected but not prefab");
                }

            }

            if (ready)
            {
                if (GUILayout.Button("Create " + gameObjects.Length + " Game Object Providers"))
                {
                    foreach (var go in gameObjects)
                    {
                        var path = AssetDatabase.GetAssetPath(go);
                        path = path.Replace(".prefab", "");
                        var gop = ScriptableObject.CreateInstance<GameObjectProvider>();
                        gop.GameObject = go;
                        AssetDatabase.CreateAsset(gop, path + "_provider.asset");

                    }
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();


                }

            }
        }





    }
}