﻿using Pidroh.UnityUtils.Misc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Pidroh.UnityUtils.Misc
{
    public class UnityEventReferences : EditorWindow
    //ScriptableWizard 
    {

        [MenuItem("Utilities/UnityEvent")]

        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(UnityEventReferences));
        }

        public string assetName;
        static string fieldN = "TextFieldScriptableObject";
        GameObject target;

        // Use this for initialization
        static void Start()
        {

        }



        void OnGUI()
        {

            GUI.SetNextControlName(fieldN);
            //assetName = EditorGUILayout.TextField("S. Asset Name:", assetName);

            //target = EditorGUILayout.ObjectField("Label:", target, typeof(GameObject), true) as GameObject;
            target = Selection.activeGameObject;

            if (target != null)
            {

                if (GUILayout.Button("Print incoming connections"))
                {
                    List<GameObject> rootObjects = new List<GameObject>();
                    Scene scene = SceneManager.GetActiveScene();
                    scene.GetRootGameObjects(rootObjects);

                    // iterate root objects and do something
                    for (int k = 0; k < rootObjects.Count; ++k)
                    {
                        GameObject gobj = rootObjects[k];
                        if (gobj != target)
                        {
                            AnalyzeObjAndChildren(gobj, target);
                        }



                    }

                }
            }

            if (GUILayout.Button("Print Outcoming connections"))
            {
                AnalyzeObj(target);
            }



            if (GUILayout.Button("Print all connections"))
            {
                // get root objects in scene
                List<GameObject> rootObjects = new List<GameObject>();
                Scene scene = SceneManager.GetActiveScene();
                scene.GetRootGameObjects(rootObjects);

                // iterate root objects and do something
                for (int k = 0; k < rootObjects.Count; ++k)
                {
                    GameObject gobj = rootObjects[k];

                    AnalyzeObjAndChildren(gobj);

                }
            }
            //Debug.Log(GUI.GetNameOfFocusedControl());
        }

        private void AnalyzeObjAndChildren(GameObject gobj, GameObject searchTarget = null)
        {
            AnalyzeObj(gobj, searchTarget);
            gobj.ActionOnAllChildren((o) =>
            {
                AnalyzeObjAndChildren(o, searchTarget);
            });
        }

        private static void AnalyzeObj(GameObject gobj, GameObject searchTarget = null)
        {
            var comps = gobj.GetComponents(typeof(Component));
            for (int i = 0; i < comps.Length; i++)
            {
                var comp = comps[i];
                const BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Public |
                BindingFlags.Instance;
                //if (obj is PageGroupManager) { //gamb
                //    var members = obj.GetType().GetMembers();
                //    foreach (var m in members)
                //    {
                //        Debug.Log(m.Name + " - " + m.MemberType);
                //    }
                //}


                FieldInfo[] fields = comp.GetType().GetFields(flags);
                //obj.GetType().GetMembers();
                foreach (FieldInfo fieldInfo in fields)
                {
                    //Debug.Log(fieldInfo.FieldType.Name);

                    //Debug.Log(fieldInfo.FieldType.IsSubclassOf(typeof(UnityEvent)));

                    bool isGeneticEvent = false;
                    if (fieldInfo.FieldType.BaseType.IsGenericType)
                    {
                        isGeneticEvent = fieldInfo.FieldType.BaseType.GetGenericTypeDefinition() == typeof(UnityEvent<>);
                    }

                    if (fieldInfo.GetValue(comp) is UnityEvent)
                    {
                        Process(gobj, searchTarget, comp, fieldInfo);
                        //fieldInfo.GetValue
                    }
                    if (isGeneticEvent)
                    {
                        Process(gobj, searchTarget, comp, fieldInfo);

                    }
                    //Debug.Log("Obj: " + obj.name + ", Field: " + fieldInfo.Name);
                }
                PropertyInfo[] properties = comp.GetType().GetProperties(flags);
                foreach (PropertyInfo propertyInfo in properties)
                {
                    if (propertyInfo.PropertyType == typeof(UnityEvent))
                    {
                        //Debug.Log("UNITY EVENT PROP!!!");
                    }
                    //Debug.Log(propertyInfo.PropertyType);
                    //Debug.Log("Obj: " + obj.name + ", Property: " + propertyInfo.Name);
                }

            }
        }

        private static void Process(GameObject gobj, GameObject searchTarget, Component comp, FieldInfo fieldInfo)
        {

            var method = fieldInfo.FieldType.GetMethod("GetPersistentEventCount");

            int count = (int)method.Invoke(fieldInfo.GetValue(comp), null);
            for (int j = 0; j < count; j++)
            {
                var target = fieldInfo.FieldType.GetMethod("GetPersistentTarget").Invoke(fieldInfo.GetValue(comp), new object[] {
                            j
                        });

                if (target is Component)
                {
                    var comp2 = target as Component;
                    if (searchTarget == null || searchTarget == comp2.gameObject)
                    {
                        Debug.Log(gobj.name + "/" + comp.GetType().Name + " ==> connected through " + fieldInfo.Name + " ==> " + comp2.gameObject.name + "/" + comp2.GetType().Name);
                    }

                }
                else
                {

                    if (target is UnityEngine.Object)
                    {
                        if (searchTarget == null || searchTarget == target as GameObject)
                        {
                            Debug.Log(gobj.name + "/" + comp.GetType().Name + " ==> connected through " + fieldInfo.Name + " ==> " + (target as UnityEngine.Object).name);
                        }
                    }
                    else
                    {
                        Debug.Log("Target no Object!!!");
                    }



                }
            }
        }

        static bool IsSubclassOfRawGeneric(Type generic, Type toCheck)
        {
            while (toCheck != null && toCheck != typeof(object))
            {
                var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
                if (generic == cur)
                {
                    return true;
                }
                toCheck = toCheck.BaseType;
            }
            return false;
        }
    }

}