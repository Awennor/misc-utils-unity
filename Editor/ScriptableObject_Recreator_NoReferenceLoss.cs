﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Pidroh.UnityUtils.Misc
{
    public class ScriptableObject_Recreator_NoReferenceLoss : EditorWindow
    {

        [MenuItem("Utilities/ScriptableAssets/Recreate (no reference loss)")]

        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(ScriptableObject_Recreator_NoReferenceLoss));
        }

        //field to add a number so it becomes unique
        string fieldId_Optional = "I...";
        //ScriptableObject origin;
        int copies = 1;
        private ScriptableObject origin;

        void OnGUI()
        {

            //assetName = EditorGUILayout.TextField("S. Asset Name:", assetName);

            //target = EditorGUILayout.ObjectField("Label:", target, typeof(GameObject), true) as GameObject;

            //fieldId_Optional = EditorGUILayout.TextField(fieldId_Optional);
            //localiName = EditorGUILayout.TextField(localiName);

            var o = EditorGUILayout.ObjectField(origin, typeof(ScriptableObject), false);
            if (o != null)
            {
                origin = o as ScriptableObject;
            }


            string directory = "Assets";
            foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets))
            {
                directory = AssetDatabase.GetAssetPath(obj);
                if (File.Exists(directory))
                {

                    directory = Path.GetDirectoryName(directory);
                }
                break;
            }
            EditorGUILayout.LabelField(directory + "/");

            if (GUILayout.Button("Recreate Scriptable Object without losing references"))
            {

                //var guis = AssetDatabase.FindAssets("t:ScriptableObject", new[] {directory});
                //foreach (var g in guis)
                //{
                var path = AssetDatabase.GetAssetPath(origin);
                var tempPathForFile = path.Replace(".asset", "TEMP.asset");
                var oldGui = AssetDatabase.AssetPathToGUID(path);
                Debug.Log(oldGui + " previous gui");
                string dataPath = Application.dataPath;

                //substring is for removing repeated assets portion
                string fullPath = dataPath + path.Substring(6, path.Length - 6);
                string fullPathTemp = dataPath + tempPathForFile.Substring(6, tempPathForFile.Length - 6);
                string metaPath = fullPath + ".meta";
                string metaPathRename = fullPath + ".metaTMP";

                Debug.Log(fullPathTemp);
                Debug.Log(File.Exists(fullPath));
                Debug.Log(File.Exists(metaPath));
                Debug.Log(File.Exists(tempPathForFile));
                Debug.Log(File.Exists(fullPathTemp));

                //AssetDatabase.CopyAsset(path, tempPathForFile);
                AssetDatabase.CreateAsset(ScriptableObject.Instantiate(AssetDatabase.LoadMainAssetAtPath(path) as ScriptableObject), tempPathForFile);
                Debug.Log(File.Exists(tempPathForFile));

                System.IO.File.Delete(fullPath);
                Debug.Log(File.Exists(fullPath));
                System.IO.File.Move(tempPathForFile, fullPath);
                Debug.Log(File.Exists(fullPath));
                Debug.Log(File.Exists(tempPathForFile));
                Debug.Log(fullPathTemp);



                //}
                AssetDatabase.Refresh();
                EditorUtility.FocusProjectWindow();

            }

        }



    }
}