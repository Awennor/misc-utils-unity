﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Pidroh.UnityUtils.Misc
{
    public class ScriptableObject_UniqueCloning : EditorWindow
    {

        [MenuItem("Utilities/ScriptableAssets/Unique cloning")]

        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(ScriptableObject_UniqueCloning));
        }

        //field to add a number so it becomes unique
        string fieldId_Optional = "Insert unique field (ID, for example) for uniquezation";
        ScriptableObject origin;
        int copies = 1;

        void OnGUI()
        {

            //assetName = EditorGUILayout.TextField("S. Asset Name:", assetName);

            //target = EditorGUILayout.ObjectField("Label:", target, typeof(GameObject), true) as GameObject;

            fieldId_Optional = EditorGUILayout.TextField(fieldId_Optional);
            //localiName = EditorGUILayout.TextField(localiName);
            Type type = typeof(ScriptableObject);
            var o = EditorGUILayout.ObjectField(origin, type, false);
            copies = EditorGUILayout.IntField(copies);
            if (o != null)
            {
                origin = o as ScriptableObject;
            }



            if (GUILayout.Button("Clone asset"))
            {
                var path = AssetDatabase.GetAssetPath(origin);
                var originPath = path;
                if (File.Exists(path))
                {
                    path = Path.GetDirectoryName(path);
                }

                for (int i = 0; i < copies; i++)
                {
                    var so = ScriptableObject.Instantiate(origin);

                    //var fields = so.GetType().GetMembers();
                    
                    //var fields = so.GetType().GetMembers(BindingFlags.NonPublic | BindingFlags.Instance);
                    //Debug.Log(fields.Length);
                    //fields = so.GetType().GetMembers(BindingFlags.FlattenHierarchy | BindingFlags.Instance);
                    //Debug.Log(fields.Length);
                    //fields = so.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
                    //Debug.Log(fields.Length);
                    //fields = so.GetType().GetFields(BindingFlags.FlattenHierarchy | BindingFlags.Instance);
                    //Debug.Log(fields.Length);
                    //for (int j = 0; j < fields.Length; j++)
                    //{
                    //    Debug.Log(fields[j].Name);
                    //}
                    var member = so.GetType().GetMember(fieldId_Optional, BindingFlags.FlattenHierarchy | BindingFlags.Instance);
                    if (member != null && member.Length > 0)
                        Debug.Log(member[0].MemberType);
                    else {
                        Debug.Log("MEMBER NOT FOUND");
                    }
                    var prop = so.GetType().GetProperty(fieldId_Optional);
                    //field.GetValue
                    if (prop != null)
                    {
                        string s = prop.GetValue(so, null) as string;
                        if (s != null)
                        {

                            prop.SetValue(so, s + "_" + i, null);
                        }
                        else {
                            Debug.Log("Not a string field");
                        }

                    }
                    else {
                        Debug.Log("Field not found");
                    }

                    string pathNow = path +"/"+ Path.GetFileNameWithoutExtension(originPath) + "_" + i + ".asset";
                    Debug.Log(Path.GetFileNameWithoutExtension(originPath));
                    Debug.Log(pathNow);
                    AssetDatabase.CreateAsset(so, pathNow);



                }

                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                EditorUtility.FocusProjectWindow();

            }

        }



    }
}