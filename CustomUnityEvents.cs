﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Events;

namespace Pidroh.UnityUtils.Misc
{
    [Serializable]
    public class IntEvent : UnityEvent<int>{}
    [Serializable]
    public class StringEvent : UnityEvent<string> { }
}
