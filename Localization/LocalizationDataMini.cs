﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using Pidroh.UnityUtils.Misc;

namespace Pidroh.UnityUtils.LocalizationMini
{
    [CreateAssetMenu(fileName = "LocalizationDataMini", menuName = "Localization/Localization Data Mini", order = 2)]
    public class LocalizationDataMini : StringProviderSO_Abstract
    {
        [SerializeField]
        LocalizationDataMiniConfig config;
        [SerializeField]
        string[] values;

        public override string GetString()
        {
            return values[Config.ChosenLanguage];
        }

        public string[] Values
        {
            get
            {
                return values;
            }

            set
            {
                values = value;
            }
        }

        public LocalizationDataMiniConfig Config
        {
            get
            {
                return config;
            }

            set
            {
                config = value;
            }
        }
    }
}