﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pidroh.UnityUtils.LocalizationMini
{
    [CreateAssetMenu(fileName = "LocalizationDataMiniConfig", menuName = "Localization/Localization Data Mini Config", order = 2)]
    public class LocalizationDataMiniConfig : ScriptableObject
    {
        [SerializeField]
        int chosenLanguage;

        public int ChosenLanguage
        {
            get
            {
                return chosenLanguage;
            }

            set
            {
                chosenLanguage = value;
            }
        }
    }
}