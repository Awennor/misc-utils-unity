﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pidroh.UnityUtils.LocalizationMini
{

    public class LocalizationDataMini_ToTextUI : MonoBehaviour
    {
        [SerializeField]
        TranslationUnit[] units;

        // Use this for initialization
        void Start()
        {
            foreach (var u in units)
            {
                u.text.text = u.locali.GetString();
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        [Serializable]
        private class TranslationUnit
        {

            public LocalizationDataMini locali;
            public Text text;
        }
    }
}