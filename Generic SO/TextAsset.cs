
using System;
using UnityEngine;

namespace Pidroh.UnityUtils.Misc
{
    [CreateAssetMenu(fileName = "TextAsset", menuName = "ScriptableObjects /TextAsset", order = 1)]
    public class TextAsset : StringProviderSO_Abstract
    {
        [SerializeField]
        string text;

        public string Text
        {
            get
            {
                return text;
            }

            set
            {
                text = value;
            }
        }

        public override string GetString()
        {
            return text;
        }
    }
}