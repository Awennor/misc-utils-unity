using UnityEngine;

namespace Pidroh.UnityUtils.Misc
{
    [CreateAssetMenu(fileName = "SpriteProvider", menuName = "ScriptableObjects /SpriteProvider", order = 1)]
    public class SpriteProvider : ScriptableObject
    {

        [SerializeField]
        Sprite sprite;
        [SerializeField]
        Vector3 scale;
        [SerializeField]
        Color color;

        public Sprite Sprite
        {
            get
            {
                return sprite;
            }

            set
            {
                sprite = value;
            }
        }

        public Vector3 Scale
        {
            get
            {
                return scale;
            }

            set
            {
                scale = value;
            }
        }

        public Color Color
        {
            get
            {
                return color;
            }

            set
            {
                color = value;
            }
        }
    }
}