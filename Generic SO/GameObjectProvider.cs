using UnityEngine;

namespace Pidroh.UnityUtils.Misc
{
    [CreateAssetMenu(fileName = "GameObjectProvider", menuName = "ScriptableObjects /GameObjectProvider", order = 1)]
    public class GameObjectProvider : ScriptableObject
    {

        [SerializeField]
        GameObject gameObject;

        public GameObject GameObject
        {
            get
            {
                return gameObject;
            }

            set
            {
                gameObject = value;
            }
        }
    }
}