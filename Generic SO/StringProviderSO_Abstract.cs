using UnityEngine;

namespace Pidroh.UnityUtils.Misc
{
    public abstract class StringProviderSO_Abstract : ScriptableObject
    {
        public abstract string GetString();
    }
}