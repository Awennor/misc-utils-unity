﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pidroh.UnityUtils.Misc
{
    public class BadUpdateLayoutsFix_Canvas : MonoBehaviour
    {

        public GameObject uiElement_OrCanvas;

        void Start()
        {
            if (isActiveAndEnabled)
                FixBug();
        }

        public void FixBug()
        {
            if (isActiveAndEnabled)
                StartCoroutine(MyCoroutine());
        }

        IEnumerator MyCoroutine()
        {

            yield return null;
            for (int i = 0; i < 2; i++)
            {

                uiElement_OrCanvas.SetActive(!uiElement_OrCanvas.activeSelf);
                yield return null;
            }


        }
    }
}